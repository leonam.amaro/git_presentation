variable "region" {
  description = "Define what region the instance will be deployed"
  default     = "us-west-1"
}

variable "associate_public_ip_address" {
  description = "Vincula um IP público à instância"
  default     = false
}

variable "name" {
  description = "Name of the application"
  default     = "CCoE-Present-Live"
}

variable "env" {
  description = "Environment of the application"
  default     = "Presentation"
}

variable "instance_type" {
  description = "AWS Instance type defines the hardware configuration of the machine"
  default     = "t3.micro"
}

variable "algo" {
  description = "It shows if the code was used correctly"
  default     = "vai dar bom"

}

variable "terragrunt" {
  description = "Used to define if the terragrunt was used correctly"
  default     = "false"
}

variable "tags" {
  description = "Map for the merging of tags"
  type        = map(string)
}




