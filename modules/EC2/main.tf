provider "aws" {
  region = var.region
}

# ----------------------------------------------------------------------------------------------------------------------
# Data sources
# ----------------------------------------------------------------------------------------------------------------------

data "aws_ami" "ubuntu" {
  most_recent = true

  owners = ["099720109477"] #Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu*amd64*"]
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# EC2 Instance
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_instance" "CCoE" {

  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  associate_public_ip_address = var.associate_public_ip_address

  tags = merge(var.tags, {
    Name        = var.name
    Environment = var.env
    Terraform   = "True"

    },
  )
}


terraform {
  backend "local" {}
}