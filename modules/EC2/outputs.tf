output "public_ip" {
  value = aws_instance.CCoE.public_ip
}

output "private_ip" {
  value = aws_instance.CCoE.private_ip
}